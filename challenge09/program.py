#!/usr/bin/env python3
import sys

def minimum_rows(ls):
    
    ones_count   = ls[0]   #amount of 1x1 legos
    twoes_count  = ls[1]   #amount of 2x1 legos
    threes_count = ls[2]   #amount of 3x1 legos
    fours_count  = ls[3]   #amount of 4x1 legos
    total        = 0       #represents the minimum# ofrows needed to hold all legos
     
    #Count the amount of 4x1's and add them to total first
    if fours_count:
        total = total + fours_count
    #Count amount of 3x1's you have and add to total
    if threes_count:
        total = total + threes_count
        #Match 3x1 blocks with all the 1x1 blocks you can
        if ones_count >= threes_count:
            ones_count = ones_count - threes_count
        else:
            ones_count = 0

    #Count amount of 2x1's you have and add to total 
    if twoes_count:
        #Try to match 2x1's with each other if possible, if not try to match a 2x1 with two 1x1's
        if twoes_count%2==0:
            total = total + (twoes_count//2)
        elif twoes_count%2!=0:
            twoes_count = twoes_count -1
            if ones_count:
                ones_count = ones_count -1 
            total = total + (twoes_count//2) + 1
        else:
            pass
    #Count 1x1's you have and finalize total
    if ones_count:
        if ones_count <=4:
            total = total + 1
        elif ones_count%4==0:
            total = total + ones_count//4
        else:
            #Case you have an odd number of 1x1 blocks and > 4
            ones_count = ones_count - 1
            total = total + ones_count//4 + 1 
    return total
      
      
#Main Execution
if __name__ == '__main__':
    for line in sys.stdin:
        line = line.rstrip().split()
        input_ls = [int(block) for block in line]
        rows = minimum_rows(input_ls)
        print(rows)
