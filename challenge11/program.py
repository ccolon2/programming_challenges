#!/usr/bin/env python3
import sys
#Goal: given a number, find the least amount of perfect squares that equate to the sum of that number
def least_num_squares(n):
    ls = [n for i in range(n+1)]
    ls[0] = 0
    i = 1
    if n==0:
        return 0
    elif n==1:
        return 1
    
    while i <  n+1:
        j = 1
        while j**2 <= i:
            ls[i] = min(ls[i],ls[i-(j**2)]+1)
            j+=1
        i+=1
   
    least_num = ls[n]
    
    return least_num
   
  


if __name__ == '__main__':
    for num in sys.stdin:
        num = int(num)
        amount = least_num_squares(num)
        print(amount)








