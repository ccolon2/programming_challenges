#!/usr/bin/env python3
import sys
#This functions returns the amount of numbers that can be created whose sum is n
def amount(n,cache={}):
    #Base cases
    if n==1:
        return 2
    elif n==2:
        return 5
    elif n==3:
        return 13
    
    #Recursive Case 
    if n not in cache:
        cache[n] = 2*amount(n-1,cache) + amount(n-2,cache) + amount(n-3,cache)
    return cache[n]

if __name__ == '__main__':
    for num in sys.stdin:
        num = int(num)
        ways = amount(num)
        print(ways)
