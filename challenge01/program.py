#!/usr/bin/env python3
import sys
def rotate(m):
    m.reverse()
    for i in range(len(m[0])):
        for j in range(i):
            m[j][i],m[i][j] = m[i][j],m[j][i] 
    return m

def print_matrix(m):
    for i in range(n):
        line = " ".join(map(str,m[i]))
        print(line)

# Main Execution
if __name__ == '__main__':
    outerlist =[]
    value = True
    is_first = True
    
    while value:
        n = int(input())
        if n==0:
            break
        for line in range(n):
            line = sys.stdin.readline().rstrip()
            sublist = line.split()
            outerlist.append(sublist)
        rotate(outerlist)
        if not is_first:
            print()
        print_matrix(outerlist)
        is_first = False

sys.exit(0)



