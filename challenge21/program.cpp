#include <climits>
#include <iostream>

using namespace std;

size_t network_num = 0;
size_t num = 0;
int lowest = 0 ;
size_t g[100][100];
size_t b[100]; 
const int big_num = INT_MAX;

bool path(size_t v, size_t t)
{
    if(v == t)
    {
        return 1;
    }
    int i = 1; 
    while(i<=num)
    {
        if( g[v][i] > 0)
        {
            if(b[i] == 0)
            {
            
                if(lowest >= g[v][i])
                {
                    lowest = g[v][i];
                }
                b[i] = v;
                if(path(i,t) == 1)
                {
                    g[v][i] -= lowest;
                    g[i][v] += lowest;
                    return 1;
                }
          
            
            }
        }
        i+=1;
    }
    return 0;
    
}
size_t max_flow(size_t t, size_t s)
{
        size_t total = 0;
        while(1)
        {
            int i = 1;
            while(i<=num)
            {
                b[i] = 0;
                i+=1;
            }
            lowest = big_num;
        
            if(path(s,t) == 0)
            {
                return total;
            }
            total += lowest;
        }
}

int main(int argc,char*argv[])
{
    while(cin >> num && num > 0)
    {
        size_t row = 1;
        size_t n1;
        size_t n2;
        size_t capacity;
        int connections;
        int s;
        int t;
       
      
        while(row<=num)
        {
           for(size_t col = 1; col <= num; col++)
           {
                g[row][col] = 0;
                                        
           }
            row+=1;
        }
        
        cin >> s;
        cin >> t;
        cin >> connections;
        int i = 0; 
        while(i < connections)
        {
            cin >> n1;
            cin >> n2;
            cin >> capacity;
            g[n1][n2] += capacity;
            g[n2][n1] += capacity;
            i+=1;
        }
    
        cout << "Network" << " " <<  ++network_num << ": Bandwidth is" << " " << max_flow(t,s) << "." << "\n";
    }
    return EXIT_SUCCESS;
}

