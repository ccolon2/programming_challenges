#include <iostream>
#include <vector>
#include <climits>
using namespace std;
const size_t NLETTERS = 26;

int count_letters(string &s1, string &s2){
    
    int count = 0;
    
    /*Impossible to make word if s2 is longer than s1*/
    if(s2.length() > s1.length()){
        return count;
    }

  
    size_t v1[NLETTERS] = {0};
    size_t v2[NLETTERS] = {0};
    
    /*Mark the letters that have been seen*/
    for(auto x: s1){v1[tolower(x) - 'a']++;}
    for(auto x: s2){ v2[tolower(x) - 'a']++;}
   
    int minimum = 0;
    int ratio;
    for(int i = 0; i < NLETTERS; i++){
        /*Cannot make the word if you do not have enough letters*/
        if(v2[i] >  v1[i]){ 
            break;
        }
        else{
            if(v2[i]!=0){
                ratio = v1[i]/v2[i];
                if(ratio < minimum || minimum==0){
                    minimum = ratio;
                }
            }
        }

    }
    
    return minimum;
}
    
/*Main Execution*/
int main(int argc, char *argv[]){
    string s1, s2;
    while(cin >> s1 >> s2){
        cout << count_letters(s1, s2) << "\n";
    }
}

