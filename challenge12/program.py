#!/usr/bin/env python3
#Code for this challenge was inspired by squirrels.py in class example
import sys

def read_grid(n):
    grid = [[0 for _ in range(n + 1)]]  # Pad grid
    for _ in range(n):
        grid.append([0] + list(map(int, sys.stdin.readline().split())))
    return grid

def minimum_kakamora(grid, n):
    #Initialize table
    big_num = sys.maxsize
    table = [[0 for _ in range(n + 1)] for _ in range(n + 1)]
    
    #Add padding
    for i in range(0,n+1):
        table[i][0] = big_num
        table[0][i] = big_num
    table[0][0] = 0
   
    

    for row in range(1, n + 1):
        for col in range(1, n + 1):
                table[row][col] = grid[row][col] + min(
                table[row][col - 1], #check left
                table[row - 1][col], #check up
                table[row-1][col-1]) #check diagonally
    return table

def find_path(grid, n, table):
    #Reconstruct the path by starting from the bottom right to the top left.
    path = []
    r, c = n, n     
   
    #Keep going until you hit spot 1,1
    while r > 0   and c > 0:
        path.append(grid[r][c])

        minimum =  min(
                table[r][c - 1], #check left
                table[r - 1][c], #check up
                table[r-1][c-1]) #check diagonally
        
        #Keep track of the indices of which cell had the minimum value so you can know what to append to the path
        row_left = r
        col_left = c - 1
        row_up   = r - 1
        col_up   = c 
        row_diag = r - 1
        col_diag = c - 1
        
        left_cell     = table[r][c-1]
        up_cell       = table[r-1][c]
        diagonal_cell = table[r-1][c-1]
        
        if minimum == left_cell:
           
            row_final = row_left
            col_final = col_left
        elif minimum == up_cell:
           
            row_final = row_up
            col_final = col_up
        else:
           
            row_final = row_diag
            col_final = col_diag
        
        c = col_final
        r = row_final

    path.reverse()
    return path

def main():
    while True:
        try:
            n = int(sys.stdin.readline())
        except ValueError:
            break
        if n==0:
            break
        grid  = read_grid(n)
       
        table = minimum_kakamora(grid, n)
        print(table[n][n])

        path  = find_path(grid, n, table)
        print(*path)
    
    sys.exit(0)

#Main execution
if __name__ == '__main__':
    main()
