#!/usr/bin/env python3
import sys
def find_longest_seq(array):
    
    history    = {} # Keeps track of index fruits were seen,  Key: cur_fruit  Value: cur_index
    final_length = 0 # Length of longest distinct seq possible
    final_seq = []
    initial_fruit_index = 0 #Tracks the first fruit in the current distinct seq 
    repeats = False
    
    for cur_index,cur_fruit in enumerate(array):
        
        if cur_fruit in history:
            length = cur_index - initial_fruit_index # Length of current distinct seq
            
            if length > final_length:
                final_seq = array[initial_fruit_index:cur_index]
            
            final_length = max(final_length,length)
            
            if history[cur_fruit] < initial_fruit_index:
                pass
            else:
                initial_fruit_index = history[cur_fruit] + 1 
            
            history[cur_fruit] = cur_index 
            
            repeats = True
        else:
            history[cur_fruit] = cur_index
    
    end_seq = array[initial_fruit_index:cur_index+1]  
    
    if len(end_seq) > len(final_seq):
        final_seq = end_seq
    
    if not repeats:
        n  = len(array)
        final_seq = array[initial_fruit_index:n]
    
    return final_seq
# Main execution
if __name__ == '__main__':
   
    for line in sys.stdin:
        line = line.rstrip()
        fruits = line.split()
        length = len(find_longest_seq(fruits))
        ordered_list = find_longest_seq(fruits)
        final_list = ", ".join(ordered_list)
        print(f'{length}: {final_list}')
