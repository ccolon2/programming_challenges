#!/usr/bin/env python3
import sys
#Goal: Find the distinct numbers you can dial in N hops from a particular position


#The solution to this problem was inspired by Alex Golec, hackernoon.com

# This map associates each number with its possible destinations after a knight move. Key: Number, Value: Destination Tuple
DESTINATION_TUPLE = {
    1: (6, 8), 
    2: (7, 9),
    3: (4, 8),
    4: (3, 9, 0),
    5: tuple(),  
    6: (1, 7, 0),
    7: (2, 6),
    8: (1, 3),
    9: (2, 4),
    0: (4, 6),
}
# This function retrieves the tuple of destinations reached starting from the specified position
def get_destinations(starting_position):
    return DESTINATION_TUPLE[starting_position]
# This function retrieves the sequences of possible numbers from a particular starting position
def find_sequences(starting_position,hops,sequence=None):
    if sequence == None:
        sequence = [starting_position]
    #Base Case
    if hops == 0:
        yield sequence
      
    
        
    else:
        for neighbor in get_destinations(starting_position):
            #Go through results of rescursive generator
            for seq in  find_sequences( neighbor, hops - 1,sequence + [neighbor] ):
                yield seq
            
def print_output(ls):
    zero = False
    new_ls=[]
    #Join all numbers in list to a single number
    for item in ls:
        x = "".join(str(n) for n in item)
        #If x starts with a 0, then we need to add a 0 to the front of that number before it is print
        if x.startswith('0'):
            zero = True
        new_ls.append(int(x))
    if not zero:
        for number in sorted(new_ls):
            print(number)
            
    else:
        for number in sorted(new_ls):
            number = str(number)
            number = '0' + number
            print(number)


#Main Execution
if __name__ == '__main__':
    is_first = True
    for line in sys.stdin:
        line = line.rstrip().split()
        line = [int(item) for item in line]
        ls = list(find_sequences(line[0],line[1]-1,sequence=None))
        zero = False
        if not is_first:
            print()
        print_output(ls)
        is_first = False
    
        

        

