#!/usr/bin/env python3
import collections
import sys
#Makes use of in class example
def detect_hamiltonian(graph):
    
    start = list(graph.keys())[0]
    visited = set()
    circuit = []
    index = 0

    visited.add(start)

    while start:
        path = find_circuit(graph, start, start, visited, [])
        if not path:
            return []
        circuit = circuit[:index] + path + circuit[index:]

        start = []
        for index, vertex in enumerate(source for source, target in circuit):
            for neighbor in graph.keys():
                if neighbor not in visited:
                    start = vertex
                    break
    return circuit

def form_graph():
    ls = list()

    g  = collections.defaultdict(dict)

    line = input().rstrip().split()
    while line[0] != "%":
        ls.append(line)
        line = input().rstrip().split()
       
    ls.sort(key=lambda l: l[0],reverse = False)
  
    ls = [[int(j) for j in i] for i in ls]
   

    for sublist in ls:
        s, t = sublist
        if s not in g:
            g[s] = list()
        if t not in g:
            g[t] = list()
        g[t].append(s)
        g[s].append(t)

    return g


def find_circuit(graph, start, vertex, visited, path):
    

    #Base Case
    if path and vertex == start:
        return path

    
    for neighbor in sorted(graph[vertex]):
        if neighbor == start:
            if len(path) >= len(graph) - 1:
                visited.add(vertex)
                path.append((vertex, neighbor))
                return path

        if neighbor in visited:
            continue

        #Mark visited
        visited.add(neighbor)
        #Add to path
        path.append((vertex, neighbor))
        #Recurse
        if find_circuit(graph, start, neighbor, visited, path):
            return path

        #Remove from path
        path.pop(-1)
        #Unmark visited
        visited.remove(neighbor)

  
    #No circuit found, so return nothing
    return []


def main():
    while sys.stdin.readline().rstrip():
        g = form_graph()
        circuit = detect_hamiltonian(g)
     
        if circuit:
            res  = []
            res.append(1)
            for source, target in circuit:
                res.append(target)
            print(" ".join(list(map(str, res))))

        else:
             print('None')
          
if __name__ == '__main__':
    main()


