#!/usr/bin/env python3
import sys

def find_target(array, target):
    empty = True
    start = 0 
    end = len(array) - 1
    while start <= end:
        middle = (start + end)//2
        midpoint = array[middle]
        lastpoint = array[end]
        firstpoint = array[start]
        
        if midpoint == target:
            return middle
        #Check if right half of array is in ascending order
        elif midpoint < firstpoint:
            
            if target > midpoint and target <= lastpoint:
                start = middle + 1
            else:
                end = middle - 1
        else:
            #Check if left half of array is in ascending order
            if target >= firstpoint and target < midpoint :
                end = middle - 1
            else:
                start = middle + 1
    
    if not empty:
        return None

#Main Execution
if __name__ == '__main__':
    for arr in sys.stdin:
        arr = arr.rstrip().split()
        arr = [int(element) for element in arr]
        target_num = int(input())
        index = find_target(arr,target_num)
        if index == None:
            print(f'{target_num} was not found')
        else:
            print(f'{target_num} found at index {index}')
    
            


