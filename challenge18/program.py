#!/usr/bin/env python3
import collections
import heapq
import sys
import math
def distance(x1,y1,x2,y2):
    return math.sqrt((x2-x1)**2 + (y2-y1)**2)
def read_graph(n):
    g       = collections.defaultdict(dict)
    ls      = []
    id_num  = 0 
    
    for line in range(n):
        x, y    = map(float,sys.stdin.readline().split())
        id_num += 1
        #Store point info and assign id_num to corresponding point
        ls.append((id_num,x,y))
        #Go through each point in list and match that point with its distance from every other point
    for id1,x1,y1 in ls:
        for id2,x2,y2 in ls:
            g[id1][id2] = distance(x1,y1,x2,y2)
            g[id2][id1] = distance(x1,y1,x2,y2)
    
    
    return g
#Create minimum spanning tree 
def minimum_road(g):
    
    frontier = []
    visited  = {}
    start    = list(g.keys())[0]
    heapq.heappush(frontier, (0, start, start))

    while frontier:
        #Get next vertex
        weight, start_node, target_node = heapq.heappop(frontier)

        #Check if node is in visited
        if target_node in visited:
            continue
        #Process node, need to know where we came from
        visited[target_node] = start_node
        #Add edges from current node to frontier
        for neighbor, cost in g[target_node].items():
            heapq.heappush(frontier, (cost, target_node, neighbor))
            
    return visited

# Main Execution
def main():
    while True:
        n = int(input()) #Number of points
        if n==0:
            break
        g   = read_graph(n)
        tree = minimum_road(g)
        e = sorted((min(s,t),max(s,t)) for s, t in tree.items() if s!=t)
        num = sum(g[s][t] for s,t in e)
        print("{:0.2f}".format(num))
        
    sys.exit(0)
       


if __name__ == '__main__':
    main()




#print(gi
