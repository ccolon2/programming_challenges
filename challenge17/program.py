#!/usr/bin/env python3
import heapq
import sys
col1 = [0, -1, 1, 0]
row1 = [-1, 0, 0, 1]
col2 = [-1, 1, -1, 1]
row2 = [-1, -1, 1, 1]

def print_result(shortest,path):
    print("Cost: {} Path: {}".format(str(shortest)," ".join(map(str,path))))
    
def ID(ls):
    return ls[0] * COLS + ls[1]

def create_path(start,end, visited):
    path = list()
    while end != start:
        path.append(ID(end))
        end = visited[end]

    path.append(ID(start))
    
    return reversed(path)

def find_locations(table):
    ending_cells      = list()
    starting_location = 0
    starting_cell     = 'S'
    ending_cell       = 'E'
    for index1, r in enumerate(table):
        for index2, c in enumerate(r):
            if c == ending_cell:
                ending_cells.append((index1, index2))
                
            elif c == starting_cell:
                starting_location = (index1, index2)
           

    return starting_location, ending_cells

def build_table(table):
    global ROWS, COLS
    rows_cols = sys.stdin.readline().split()
    ROWS,COLS = int(rows_cols[0]), int(rows_cols[1])
    
    #Build table

    for row in range(ROWS):
        table.append([])
        rows = sys.stdin.readline().split()
        for cell in rows:
            if cell == '1':
                table[row].append(0)
            elif cell == '0':
                table[row].append(1)
            else:
                table[row].append(cell)
    return table


def find_cost(table, start, cell):
    stack = [(0, start, start)]
    visited = dict()
    while stack:
        cost, start_node, end_node = heapq.heappop(stack)
        
        if end_node in visited:
            continue
        
        visited[end_node] = start_node

        if end_node == cell:
            return  cost,visited

        for index in range(len(col1)):
          
            
            col = col1[index] + end_node[1]
            row = row1[index] + end_node[0]
            in_bounds = col < COLS and row < ROWS and row >= 0 and col >= 0 
            if in_bounds:
                if table[row][col]:
                    if (row, col) == cell:
                        visited[(row, col)] = end_node
                      
                        return  cost + 1, visited
                    heapq.heappush(stack, (cost + 1, end_node, (row, col)))
        
            
            col = col2[index] + end_node[1] 
            row = row2[index] + end_node[0] 
            in_bounds = col < COLS and row < ROWS and row >= 0 and col >= 0 

            if in_bounds:
                if table[row][col]: 
                    if (row, col) == cell:
                        visited[(row, col)] = end_node
                        return cost + 2,visited
                    heapq.heappush(stack, (cost + 2, end_node, (row, col)))

    
    return False,visited



def main():
    while True:
        shortest   = sys.maxsize
        grid = []
        ls = []
        table  = build_table(grid)
        
        start,end_locations = find_locations(table)
        if not table:
            break
        for cell in end_locations:
            cost,visited = find_cost(table, start, cell) 
            if not cost:
                if len(ls) < 1:
                    shortest = sys.maxsize
            elif cost < shortest:
                path = create_path(start, cell, visited)
                ls.append(path)
                shortest = cost
        if shortest == sys.maxsize:
            print('Cost: 0 Path: None')
            continue
        
        print_result(shortest,path)
       
    sys.exit(0)

if __name__ == "__main__":
    main()
