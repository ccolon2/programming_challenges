#!/usr/bin/env python3
import sys

def get_html_tag(html_string):
    tag = ''
    tag_mode = False
    for element in html_string:
        if tag_mode:
            #We are now in tag_mode, so build tag string until ">" is hit
            if element == ">":
                yield tag
                tag_mode = False
            tag = tag + element
        else:
            #We are not in tag_mode, so if "<" is hit, enter tag mode
            if element == "<":
                tag = ''
                tag_mode = True

def is_balanced(html_string):
    stack = []
    
    for tag in get_html_tag(html_string):
        if not tag.startswith( '/' ):
            stack.append(tag)
        else:
            if not stack:
                return False
            else:
                matching_tag = stack.pop(-1) 
                if matching_tag != tag[1:]:
                    return False
    return not stack

# Main Execution
if __name__ == '__main__':
    for line in sys.stdin:
        line = line.rstrip()
        if is_balanced(line):
            print("Balanced")
        else:
            print("Unbalanced")
   
    
    


