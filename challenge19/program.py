#!/usr/bin/env python3
import sys
import collections

Graph = collections.namedtuple('Graph', 'edges degrees')
def topological_sort(graph):
    frontier = [v for v, d in graph.degrees.items() if d == 0]
    visited = []

    while frontier:
        vertex = frontier.pop()
        visited.append(vertex)

        for neighbor in graph.edges[vertex]:
            graph.degrees[neighbor] -= 1
            if graph.degrees[neighbor] == 0:
                frontier.append(neighbor)

    return visited


def read_graph():
   
    visited = list()
    edges   = collections.defaultdict(set)
    degrees = collections.defaultdict(int)

    for line in sys.stdin:
        entry = list(line.rstrip())

           
        first,second,third = entry[0], entry[1], entry[2]

       
        edges[second].add(third)
        edges[first].add(second)
      
        if (second, third) not in visited:
            degrees[third] += 1
        if (first,second) not in visited:
            degrees[second] += 1
        visited.append((first, second))
        visited.append((second, third))
        degrees[first]

    return Graph(edges, degrees)

def main():
    g  = read_graph()
    s_g= topological_sort(g) 
    if len(s_g) == len(g.degrees):
        print("".join(s_g))
    else:
        print('There is a cycle!')
    



if __name__ == '__main__':
    main()
 
     

