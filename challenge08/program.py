#!/usr/bin/env python3
import itertools
import sys
#Goal: Generate all combinations of length n with k 1's 
def generate_binary(n, k):
    combos_ls = []
    binary_string=[]
    #Produces tuples of the indices k '1's could be placed 
    combos = itertools.combinations(range(n),k)
    for index_tuple in combos:
        #Initialize string to all '0's
        binary_string = ['0']*n
        #Each index_tuple contains the index at which a 1 could be placed.
       
        for index in index_tuple:
            #Replace necessary spots with 1's
            binary_string[index] = '1'
        joined_binary = ''.join(binary_string)
        combos_ls.append(joined_binary)
    return combos_ls

def max_length(list1,list2):
    starting_length = len(list1[0])
    for i in range(len(list2)):
        if len(list1[i]) >= starting_length:
                max_length = len(list1[i])
    return max_length

def print_output(final_ls,length,n):
    item=[]
    if len(final_ls)==1:
        item = final_ls[0].zfill(n)
        print(item)


    else:
        for item in final_ls:
            #Compare each element to max_length, if it is not max_length, then add '0' to beginning
            if len(item) < length:
                item = item.zfill(length) 
            else:
                pass
            print(item)
   
#Main Execution
if __name__ == '__main__':

    is_first = True
    for line in sys.stdin:
        line = line.rstrip().split()
        line = [int(item) for item in line]
        bit_ls = []
        str_ls =[]
        n = line[0]
        k = line[1]
        #Generate the list of possible binary strings
        result = generate_binary(n,k)
        
        #Convert each item in bit_ls to an integer so it can be sorted in ascending order.
        bit_ls = [int(item) for item in result]
        
        #Sort bits in ascending order
        sorted_ls = sorted(bit_ls)
        
        #Once sorted, you must convert items in list back to a string so that you can add a '0' where necessary
        str_ls = [str(item) for item in sorted_ls]
        
        #Obtain length of longest string, so you can figure out how many zeroes to add to strings that have a length less than max_length
        length = max_length(str_ls,bit_ls)
    
        if not is_first:
            print()
            
        print_output(str_ls,length,n) 
        is_first = False
    
    
    
        

