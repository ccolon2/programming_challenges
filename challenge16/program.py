#!/usr/bin/env python3
import sys
def read_graph(n, m):
    #Create adjacency set
    g = {v: list() for v in range(n)}

    for _ in range(m):
        s, t = map(int, sys.stdin.readline().split())
    
        g[s].append(t)
        g[t].append(s)

    return g

def longest_road(g, n):
    max_length = 0
    frontier = [(0, n, n)] #Intialize stack with first node
    visited = set()        #Keep track of nodes that have been seen
    prev_length = 0
    #While there are still tuples in the frontier...
    while frontier:
        #Pop tuple, which stores length, start_node, end_node
        length, end_node, start_node = frontier.pop()
        #Check if edge has been visited
        if (start_node,end_node) in visited:
            continue
        elif (end_node,start_node) in visited:
            continue
        else:
            pass
        #Mark the edge as visited if it has not been visited yet
        visited.add((start_node,end_node))
        #Look at neighbors of the end_node and update length
        max_length = max(max_length,length)
        for neighbor in g[end_node]:
            frontier.append((length+1, neighbor,end_node))
    return max_length  
def determine_max(ls,g):
    #Obtain maximum length starting from each distinct node
    maximum = 0
    for node in ls:
        maximum = max(maximum,longest_road(g,node))
    return maximum 
# Main execution
def main():
    n, m = map(int, sys.stdin.readline().split())
    while n and m:
        g = read_graph(n, m)
        x = list(g.keys())
        result = determine_max(x,g)
        print(result)
        n, m = map(int, sys.stdin.readline().split())
        if n == 0 and m ==0:
            break
    sys.exit(0)

if __name__ == '__main__':
    main()
