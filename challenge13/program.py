#!/usr/bin/env python3
import sys
class Node:
    def __init__(self,value, left = None, right = None):
        self.value = value
        self.left  = left
        self.right = right
#Convert array to binary search tree
def convert_array(arr):
    if not arr:
        return 
    mid        = len(arr)//2
    root       = Node(arr[mid]) 
    root.right = convert_array(arr[mid+1:])
    root.left  = convert_array(arr[:mid])
    return root
#Display output using breadth first traversal
def print_output(root):
    queue = [root]
    while(queue):
        items = len(queue)
        row = []
        while items > 0:
            node = queue.pop(0)
            row.append(node.value) 
            if node.left:
                queue.append(node.left)
            if node.right:
                queue.append(node.right)
            items -= 1
        row = [str(i) for i in row]
     
        print(" ".join(row))
       

if __name__ == '__main__':
    for line in sys.stdin:
        nodes = [int(num) for num in line.split()]
        root  = convert_array(nodes)
        print_output(root)
  
